﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace todolist
{

    public class Task : INotifyPropertyChanged
    {
        // Data fields
        private static uint _taskCounter = 1;
        private string      _title;
        private string      _content;
        private string      _contentResume;
        private bool?       _done;
        private DateTime    _dueDate;

        public string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string Content
        {
            get { return _content; }
            set
            {
                if (value != _content)
                {
                    _content = value;
                    if (this.Content.Length > 10)
                    {
                        this.ContentResume = _content.Substring(0, 10);
                        this.ContentResume += "...";
                    }
                    else
                    {
                        this.ContentResume = this.Content;
                    }
                    NotifyPropertyChanged();
                }
            }
        }

        public bool? Done
        {
            get { return _done; }
            set
            {
                if (value != _done)
                {
                    _done = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                if (value != _dueDate)
                {
                    _dueDate = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string ContentResume
        {
            get { return _contentResume; }
            set
            {
                if (value != _contentResume)
                {
                    _contentResume = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime CreationDate { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Task(DateTime creationDate, DateTime dueDate, string content)
        {
            this.Title = "Task - " + _taskCounter++;
            this.Content = content;
            this.Done = false;
            
            this.CreationDate = creationDate;
            this.DueDate = dueDate;
        }

        public Task(DateTime creationDate, DateTime dueDate, string title, string content)
        {
            this.Title = title;
            this.Content = content;
            this.Done = false;
            if (this.Content.Length > 10)
            {
                this.ContentResume = content.Substring(0, 10);
                this.ContentResume += "...";
            }
            else
            {
                this.ContentResume = this.Content;
            }
            this.CreationDate = creationDate;
            this.DueDate = dueDate;
        }

    }
}