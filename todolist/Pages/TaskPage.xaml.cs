﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System;

namespace todolist
{
    public sealed partial class TaskPage : Page
    {
        private Task _currentTask;

        public TaskPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is Task && e.Parameter != null)
            {
                _currentTask = e.Parameter as Task;
                taskTitle.Text = _currentTask.Title;
                taskContent.Text = _currentTask.Content;
                taskDuedate.Date = _currentTask.DueDate;
            }
            base.OnNavigatedTo(e);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        private async void DeleteTask(object sender, RoutedEventArgs e)
        {
            MessageDialog showDialog = new MessageDialog("Are you sure to delete this task ?");
            showDialog.Commands.Add(new UICommand("Delete") { Id = 0 });
            showDialog.Commands.Add(new UICommand("Cancel") { Id = 1 });
            showDialog.DefaultCommandIndex = 0;
            showDialog.CancelCommandIndex = 1;
            var result = await showDialog.ShowAsync();
            if ((int)result.Id == 0)
            {
                Frame.Navigate(typeof(MainPage), new DeleteTask());
            }
        }

        private void SaveTask(object sender, RoutedEventArgs e)
        {
            _currentTask.Title = taskTitle.Text;
            _currentTask.Content = taskContent.Text;
            _currentTask.DueDate = taskDuedate.Date.Date;
            // TODO : Display a visual effect to notify the user
        }
    }
}
