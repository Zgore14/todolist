﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Navigation;

namespace todolist
{

    public sealed partial class MainPage : Page
    {
        public ObservableCollection<Task> Tasks { get; set; }

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Enabled;
            Tasks = new ObservableCollection<Task>();
            this.DataContext = Tasks;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is DeleteTask
                && e.Parameter != null
                && TasksListView.Items.Count > 0
                && TasksListView.SelectedIndex >= 0)
            {
                var data = e.Parameter as DeleteTask;
                Tasks.RemoveAt(TasksListView.SelectedIndex);
            }
            TasksListView.SelectedItem = null;
            base.OnNavigatedTo(e);
        }

        private void AddTaskButton_Click(object sender, RoutedEventArgs e) {}

        private void CleanCreateTaskFields()
        {
            taskTitle.Text = string.Empty;
            taskContent.Text = string.Empty;
            taskDuedate.Date = DateTime.Now;
        }

        private void CreateTask(object sender, RoutedEventArgs e)
        {
            var f = this.AddTaskButton.Flyout as Flyout;
            var grid = f.Content as Grid;
            var elems = grid.Children;
            Task task;

            if (taskDuedate.Date < DateTime.Now)
            {
                taskDuedate.Date = DateTime.Now;
                // TODO : Notify the user that he can't enter a due date in the past
            }

            if (taskTitle.Text.Equals(string.Empty))
            {
                task = new Task(DateTime.Now, taskDuedate.Date.Date, taskContent.Text.ToString());
            }
            else
            {
                task = new Task(DateTime.Now, taskDuedate.Date.Date, taskTitle.Text.ToString(), taskContent.Text.ToString());
            }

            Tasks.Add(task);
            System.Diagnostics.Debug.WriteLine(Tasks.Count);
            

            this.CleanCreateTaskFields();

            f.Hide();
        }

        private void TasksListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tasksList = sender as ListView;
            // task is null when the user sort the list
            if (tasksList.SelectedItem is Task task)
            {
                System.Diagnostics.Debug.WriteLine(task.Title);
                Frame.Navigate(typeof(TaskPage), task);
                return;
            }
            System.Diagnostics.Debug.WriteLine("Not a task");
            return;
        }

        private void RPDone_Checked(object sender, RoutedEventArgs e)
        {
            var c = sender as CheckBox;
            if (c.Tag is Task t)
            {
                t.Done = true;
            }
        }

        private void RPDone_Unchecked(object sender, RoutedEventArgs e)
        {
            var c = sender as CheckBox;
            if (c.Tag is Task t)
            {
                t.Done = false;
            }
        }
    }
}
